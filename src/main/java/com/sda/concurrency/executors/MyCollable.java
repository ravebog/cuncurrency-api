package com.sda.concurrency.executors;

import java.util.concurrent.Callable;

public class MyCollable implements Callable<String> {

    @Override
    public String call() throws Exception {
        int sum=0;
        for (int i = 1; i< 10; i++){
            sum+=i;
            System.out.println(sum);
        }

        return "Suma este: "+sum;
    }
}
