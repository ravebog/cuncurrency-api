package com.sda.concurrency.executors;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SingleThreadExecutorDemo {
    public static void main(String[] args) {
        ExecutorService service = null;
        try{
            service = Executors.newSingleThreadExecutor();
            System.out.println("before");
            service.execute(new SumRunnable());
            Future<?> submit = service.submit(new SumRunnable());
            while(!submit.isDone()){
                System.out.println("Waiting");
                Thread.sleep(100);
            }
            Future<String> result = service.submit(new MyCollable());
            System.out.println(result.get());


            System.out.println("after");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            if(service!=null){
                service.shutdown();
            }
        }
    }
}
