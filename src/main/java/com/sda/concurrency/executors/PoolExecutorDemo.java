package com.sda.concurrency.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolExecutorDemo {
    public static void main(String[] args) {
        ExecutorService service=null;
        try{
            int cores = Runtime.getRuntime().availableProcessors();
            service= Executors.newFixedThreadPool(2*cores);
            service.submit(new MyCollable());
            service.submit(new MyCollable());
            service.submit(new MyCollable());

        }finally {
            if (service!=null){
                service.shutdownNow();
            }
        }
    }
}
