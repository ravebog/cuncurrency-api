package com.sda.concurrency.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorDemo {
    public static void main(String[] args) {
        ScheduledExecutorService service = null;
        try{
            service = Executors.newSingleThreadScheduledExecutor();
            service.schedule(()-> System.out.println("Hello"),10, TimeUnit.SECONDS);
            service.scheduleAtFixedRate(()-> System.out.println("Hi"),1,2, TimeUnit.SECONDS);
        }finally {
//            if(service!=null){
//                service.shutdown();
//            }
        }
    }
}
