package com.sda.concurrency.executors;

public class SumRunnable implements Runnable {
    @Override
    public void run() {
        int sum = 0;
        for (int i = 1; i < 10; i++) {
            sum += i;
            System.out.println(sum);
        }
    }
}
