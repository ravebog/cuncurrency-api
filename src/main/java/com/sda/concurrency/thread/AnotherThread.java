package com.sda.concurrency.thread;

public class AnotherThread extends Thread{
    private String name;

    public AnotherThread(String threadName, String name) {
        super(threadName);
        this.name = name;
    }

    public void run(){
        System.out.println("Thread name: "+Thread.currentThread().getName());
        System.out.println("My name: "+name);
    }


}
