package com.sda.concurrency.thread;

public class ThreadDemo {
    public static void main(String[] args) {
        System.out.println("before");
        MyThread myThread = new MyThread();
        myThread.setName("George");
        myThread.start();

        Thread.currentThread().setName("Ion");
        for(int i=10;i<20;i++){
            System.out.println(Thread.currentThread().getName()+":"+i);
        }
        System.out.println("after");
    }
}
