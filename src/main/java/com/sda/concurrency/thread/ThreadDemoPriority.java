package com.sda.concurrency.thread;

public class ThreadDemoPriority {
    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        MyThread t4 = new MyThread();

        t1.setName("Ion");
        t2.setName("George");
        t3.setName("Mihai");
        t4.setName("Dan");

        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t3.setPriority(Thread.NORM_PRIORITY);
        t4.setPriority(4);

        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("********* "+cores+" **********");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }

}
