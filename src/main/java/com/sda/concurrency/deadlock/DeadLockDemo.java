package com.sda.concurrency.deadlock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DeadLockDemo {
    public static void main(String[] args) {
        ExecutorService service = null;
        try{
            service = Executors.newFixedThreadPool(2);
            Printer printer = new Printer();
            Scanner scanner = new Scanner();
            PrintAndScan p1 = new PrintAndScan(printer,scanner);
            ScanAndPrint p2 = new ScanAndPrint(printer,scanner);
            service.execute(()->p1.printAndScan());
            service.execute(()->p2.scanAndPrint());

        }finally {
            if(service!=null){
                service.shutdown();
            }
        }
    }
}
