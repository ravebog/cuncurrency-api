package com.sda.concurrency.deadlock;

public class ScanAndPrint {
    private Printer printer;
    private Scanner scanner;

    public ScanAndPrint(Printer printer, Scanner scanner) {
        this.printer = printer;
        this.scanner = scanner;
    }

    public void scanAndPrint() {
        System.out.println("before scanning!");
        synchronized (scanner) {
            scanner.scan();
            System.out.println("after scanning!");
            System.out.println("Before printing");

            synchronized (printer) {
                printer.print();
            }

            System.out.println("after printing");
        }
    }


}
