package com.sda.concurrency.deadlock;

public class PrintAndScan {
    private Printer printer;
    private Scanner scanner;

    public PrintAndScan(Printer printer, Scanner scanner) {
        this.printer = printer;
        this.scanner = scanner;
    }

    public void printAndScan() {
        System.out.println("Before print 2");
        synchronized (printer) {
            printer.print();
            System.out.println("After print 2");
            System.out.println("Before Scan 2");

            synchronized (scanner) {
                scanner.scan();
            }

            System.out.println("After scan 2");

        }
    }


}
