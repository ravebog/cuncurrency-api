package com.sda.concurrency.threadmethods;

public class SayHello implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Hello!");
        }
    }
}
