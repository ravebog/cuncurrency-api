package com.sda.concurrency.threadmethods;

public class SayHi implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Hi");
            if (i == 5) {
                new Thread(new SayHello()).start();

            }
        }
    }
}
