package com.sda.concurrency.threadmethods;

public class ThreadSleepDemo {
    public static void main(String[] args) {
        System.out.println("before");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("after");
    }
}
