package com.sda.concurrency.threadmethods;

public class SayBuna implements Runnable {
    @Override
    public void run() {
        for(int i=0;i<100;i++){
            System.out.println("Buna!");
            if(i==20){
                Thread thread = new Thread(new SayHello());
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
