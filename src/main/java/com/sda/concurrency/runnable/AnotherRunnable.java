package com.sda.concurrency.runnable;

public class AnotherRunnable implements Runnable {
    @Override
    public void run() {
        new SayHi().say();
    }
}
