package com.sda.concurrency.runnable;

public class MyRunnable implements Runnable{
    private String hello;

    public MyRunnable(String hello) {
        this.hello = hello;
    }

    @Override
    public void run() {

        for (int i =0; i<10; i++){
            System.out.println(hello);
        }

    }
}
