package com.sda.concurrency.runnable;

public class RunnableDemo {
    public static void main(String[] args) {
        Runnable r1 = new MyRunnable("Hello");
        Runnable r2 = new MyRunnable("Hi");

        new Thread(r1).start();
        new Thread(r2).start();

        new Thread(() -> System.out.println("Salut !")).start();

        new Thread(() -> new SayHi().say()).start();
        new Thread(new AnotherRunnable()).start();



    }
}
