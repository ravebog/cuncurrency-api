package com.sda.concurrency.syncronization;

public class IncrementAndGet implements Runnable {
    private int counter;
    //private Object lock = new Object();


    @Override
    synchronized public void run() {
        //dureaza mult

        System.out.println("before");

        //dureaza putin

        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.out.println(++counter);
            }
        }
        System.out.println("after");
        //taskuri care dureaza mult

    }
}
