package com.sda.concurrency.syncronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SyncDemo {
    public static void main(String[] args) {
        ExecutorService service=null;
        try{
            service = Executors.newFixedThreadPool(4);
            Runnable r = new IncrementAndGet();
            service.execute(r);
            service.execute(r);
            service.execute(r);
            service.execute(r);
        }finally {
            if(service!=null){
                service.shutdownNow();
            }
        }
    }
}
